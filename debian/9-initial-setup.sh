#!/bin/bash

# @todo convert the following into a Ansible playbook

# on the new box, logged in as root
adduser mark
usermod -aG sudo mark
apt update
apt install ufw
ufw allow OpenSSH
ufw enable
ufw status
cp -r ~/.ssh /home/mark
chown -R mark:mark /home/mark/.ssh
sudo apt install man-db
sudo apt install vim
sudo update-alternatives --config editor
sudo apt install zsh
sudo chsh zsh