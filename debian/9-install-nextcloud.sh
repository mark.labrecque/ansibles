#!/bin/bash

# @todo convert the following into a Ansible playbook

# log in as your personal user
sudo apt update
sudo apt install snapd
echo 'export PATH=$PATH:/snap/bin' >> ~/.bashrc
source ~/.bashrc
source /etc/profile.d/apps-bin-path.sh
sudo snap install nextcloud
# NextCloud username and password defined here (replace USERNAME and PASSWORD in the following command)
sudo -i nextcloud.manual-install USERNAME PASSWORD
# Setup your trusted domain by replacing DOMAIN_NAME in the following command
sudo -i nextcloud.occ config:system:set trusted_domains 1 --value=DOMAIN_NAME
sudo ufw allow "WWW Full"
sudo -i nextcloud.enable-https lets-encrypt